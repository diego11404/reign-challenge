import { Controller, Post } from '@nestjs/common';
import { HackerNewsService } from './modules/hacker-news/hacker-news.service';
import { Response } from './modules/hacker-news/interfaces/response.interface';
import { responseError, responseOk } from './utils/functions';

@Controller()
export class AppController {
  constructor(private readonly hnService: HackerNewsService) {}

  @Post('populate')
  async populateData(): Promise<Response> {
    let resp = 'error';
    try {
      resp = await this.hnService.populate();

      return responseOk(resp);
    } catch (ex) {
      console.log('Error: ', ex);
    }
    return responseError(resp);
  }
}
