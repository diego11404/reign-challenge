import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { schedule } from 'node-cron';
import { AppModule } from './app.module';
import { HackerNewsService } from './modules/hacker-news/hacker-news.service';
import { EVERY_HOUR } from './utils/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = new DocumentBuilder()
    .setTitle('Reign challenge')
    .setDescription('nestjs and mongoDB')
    .setVersion('1.0')
    .addTag('')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000).then(() => {
    schedule(EVERY_HOUR, async () => {
      const service = app.get<HackerNewsService>(HackerNewsService);
      service.insertFromCronJob();
    });
  });
}

bootstrap();
