import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { HackerNewsModule } from './modules/hacker-news/hacker-news.module';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://${process.env.USER_DB}:${process.env.USER_DB}@${process.env.HOST_DB}:27017/reign`),
    HackerNewsModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
