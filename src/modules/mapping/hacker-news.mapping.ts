import { HackerNewsInterface } from '../hacker-news/interfaces/hack-news.interface';
import { HackerNews } from '../hacker-news/schemas/hacker-new.schema';

export function hackerNewsfromDto(dto: HackerNewsInterface): HackerNews {
  const newObj = new HackerNews();
  newObj.createdAt = dto.created_at;
  (newObj.title = dto.title), (newObj.url = dto.url);
  newObj.author = dto.author;
  newObj.points = dto.points;
  newObj.storyText = dto.story_text;
  newObj.commentText = dto.comment_text;
  newObj.numComments = dto.num_comments;
  newObj.storyId = dto.story_id;
  newObj.storyTitle = dto.story_title;
  newObj.tags = dto._tags;
  newObj.objectID = dto.objectID;
  newObj.highlightResult = dto._highlightResult;

  return newObj;
}
