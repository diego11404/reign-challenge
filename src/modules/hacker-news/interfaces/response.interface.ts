export interface Response {
  error: boolean;
  data: any;
}
