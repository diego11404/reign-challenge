import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  HackerNews,
  HackerNewsSchema,
} from 'src/modules/hacker-news/schemas/hacker-new.schema';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsService } from './hacker-news.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: HackerNews.name, schema: HackerNewsSchema },
    ]),
  ],
  controllers: [HackerNewsController],
  providers: [HackerNewsService],
  exports: [HackerNewsService],
})
export class HackerNewsModule {}
