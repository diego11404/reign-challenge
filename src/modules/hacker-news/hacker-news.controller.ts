import { Controller, Delete, Get, Header, Param, Query } from '@nestjs/common';
import { responseOk, responseError } from 'src/utils/functions';
import { HackerNewsService } from './hacker-news.service';
import { Response } from './interfaces/response.interface';
import { HnDocument } from './schemas/hacker-new.schema';
import { ApiParam, ApiParamOptions } from '@nestjs/swagger';
@Controller('news')
export class HackerNewsController {
  constructor(private readonly service: HackerNewsService) {}

  @Get()
  @Header('content-type', 'application/json')
  @ApiParam({ name: 'author', required: false })
  @ApiParam({ name: 'tags', required: false })
  @ApiParam({ name: 'title', required: false })
  @ApiParam({ name: 'month', required: false })
  @ApiParam({ name: 'page', required: false })
  async getByFilter(
    @Query('author') author,
    @Query('tags') tags,
    @Query('title') title,
    @Query('month') month,
    @Query('page') page,
  ): Promise<Response> {
    try {
      let resp: HnDocument[];
      if (!author && !tags && !title && !month && !page) {
        resp = await this.service.getAll();
      } else {
        resp = await this.service.searchHackerNews(
          author,
          tags,
          title,
          month,
          page,
        );
      }

      return responseOk(resp);
    } catch (ex) {
      console.log('Error: ', ex);
    }
    return responseError('error');
  }

  @Delete('/:id')
  @Header('content-type', 'application/json')
  @ApiParam({ name: 'id' })
  async removeById(@Param('id') id): Promise<Response> {
    try {
      await this.service.removeByID(id);
      return responseOk('done');
    } catch (ex) {
      console.log('Error: ', ex);
    }
    return responseError('error');
  }
}
