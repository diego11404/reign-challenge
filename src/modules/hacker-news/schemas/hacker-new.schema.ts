import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { HighlightResult } from 'src/modules/hacker-news/interfaces/hack-news.interface';

export type HnDocument = HackerNews & Document;

@Schema()
export class HackerNews {
  @Prop()
  createdAt: Date;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: string;

  @Prop()
  storyText: string;

  @Prop()
  commentText: string;

  @Prop()
  numComments: number;

  @Prop()
  storyId: number;

  @Prop()
  storyTitle: string;

  @Prop()
  storyUrl: string;

  @Prop([String])
  tags: string[];

  @Prop()
  objectID: string;

  @Prop(raw({}))
  highlightResult: HighlightResult;
}

export const HackerNewsSchema = SchemaFactory.createForClass(HackerNews);
