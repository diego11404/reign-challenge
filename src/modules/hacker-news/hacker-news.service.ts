import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { lastValueFrom } from 'rxjs';
import { HackerNewsInterface } from 'src/modules/hacker-news/interfaces/hack-news.interface';
import {
  HackerNews,
  HnDocument,
} from 'src/modules/hacker-news/schemas/hacker-new.schema';
import { MONTH_NAMES, TARGET_ENDPOINT } from 'src/utils/constants';
import { hackerNewsfromDto } from '../mapping/hacker-news.mapping';
import * as moment from 'moment';

@Injectable()
export class HackerNewsService {
  constructor(
    private readonly httpService: HttpService,
    @InjectModel(HackerNews.name) private hnModel: Model<HnDocument>,
  ) {}

  async populate(): Promise<string> {
    const { data } = await lastValueFrom(this.httpService.get(TARGET_ENDPOINT));

    const hits: HackerNewsInterface[] = data.hits;

    await this.hnModel.deleteMany({});

    this.createDocuments(hits);

    return 'done';
  }

  async getAll() {
    return this.hnModel.find({});
  }

  async insertFromCronJob() {
    console.log('inserting new documents from cron');
    const { data } = await lastValueFrom(this.httpService.get(TARGET_ENDPOINT));

    const hits: HackerNewsInterface[] = data.hits;

    this.createDocuments(hits);
  }

  async searchHackerNews(
    author: string,
    tag: string,
    title: string,
    month: string,
    page: number,
  ) {
    console.log('filters: ', arguments);
    const perPage = 5;
    const filter: any = { $or: [] };

    if (!!author && author != '') {
      filter['$or'].push({ author: author });
    }

    if (!!tag && tag != '') {
      filter['$or'].push({ tags: { $in: tag.split(',') } });
    }

    if (!!title && title != '') {
      filter['$or'].push({ title: title });
    }

    if (!!month && month != '') {
      const isValid = MONTH_NAMES.some((e) => {
        return e.toLowerCase() == month.toLowerCase();
      });

      if (isValid) {
        const n = moment().month(month).format('M');
        const n2 = Number(n) + 1;
        const init = new Date(`2022-${n}-01 00:00:00`);
        const end = new Date(`2022-${n2}-01 00:00:00`);
        console.log('init: ', init, 'end: ', end);
        filter['$or'].push({ createdAt: { $gte: init, $lt: end } });
      }
    }

    console.log('criteria: ', filter);

    return this.hnModel
      .find(filter['$or'].length > 0 ? filter : {})
      .limit(perPage)
      .skip((page - 1) * perPage);
  }

  createDocuments(hits: HackerNewsInterface[]) {
    hits.forEach((e) => {
      const document = new this.hnModel(hackerNewsfromDto(e));
      document.save().catch((err) => console.log('error inserting docs', err));
    });
  }

  async removeByID(id: string) {
    return this.hnModel.deleteOne({ _id: id });
  }
}
