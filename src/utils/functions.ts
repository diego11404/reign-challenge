export function responseOk<T>(obj: T) {
  return {
    error: false,
    data: obj,
  };
}

export function responseError<T>(obj: T) {
  return {
    error: true,
    data: obj,
  };
}
