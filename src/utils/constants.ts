export const EVERY_10_SECONDS = '*/10 * * * * *';
export const EVERY_HOUR = '0 0 * * * *';
export const TARGET_ENDPOINT =
  'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

export const MONTH_NAMES = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
