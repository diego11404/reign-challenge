FROM node:fermium-alpine as build

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

RUN npm install glob rimraf

COPY . .

RUN npm run build


FROM node:fermium-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --production

COPY . .

COPY --from=build /usr/src/app/dist ./dist

CMD ["node", "dist/main"]