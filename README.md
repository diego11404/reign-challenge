# install
## 1. build images

```bash
 docker-compose build
```

## 2. run containers
```bash
 docker-compose up
```

## The server is running on
```
http://localhost:3000/news
```

## API Doc Swagger
```
http://localhost:3000/api
```

---
<br>

## Polulate data for first time
```bash
 POST http://localhost:3000/populate
```